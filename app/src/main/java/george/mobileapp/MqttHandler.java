package george.mobileapp;

import android.content.Context;
import android.util.Log;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONException;
import org.json.JSONObject;

public class MqttHandler {
    private Context context;
    private MqttAndroidClient mqttClient;

    MqttHandler(Context context) {
        this.context = context;
        String clientId = "george"+System.currentTimeMillis();
        this.mqttClient = new MqttAndroidClient(this.context, this.context.getResources().getString(R.string.remote_broker_uri), clientId, new MemoryPersistence(), MqttAndroidClient.Ack.AUTO_ACK);
        initClient(this.mqttClient);

    }

    private void initClient(final MqttAndroidClient mqttClient) {
        mqttClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectionLost(Throwable cause) {
                Log.d("logger", "connection lost");
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                byte[] payload = message.getPayload();
                JSONObject jsonPayload = new JSONObject(new String(payload));
                RiderActivity riderActivity = (RiderActivity) context;
                riderActivity.setState(jsonPayload.get("state").toString(), jsonPayload.get("message").toString());
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
                //
            }

            @Override
            public void connectComplete(boolean reconnect, String serverURI) {
                if (reconnect) {
                    Log.d("logger", "reconnected");
                    // Clean session true - must re-subscribe
                    subscribeToTopic(mqttClient);
                } else {
                    subscribeToTopic(mqttClient);
                    Log.d("logger", "first time connected");
                }
            }
        });

        String username = this.context.getResources().getString(R.string.remote_broker_username);
        String password = this.context.getResources().getString(R.string.remote_broker_password);

        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(true);
        mqttConnectOptions.setUserName(username);
        mqttConnectOptions.setPassword(password.toCharArray());
        mqttConnectOptions.setConnectionTimeout(300);
        mqttConnectOptions.setKeepAliveInterval(300);

        try {
            mqttClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
//                    subscribeToTopic();
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.d("logger", "failed to connect");
                }
            });
        } catch (MqttException ex) {
            ex.printStackTrace();
        }
    }

    private void subscribeToTopic(MqttAndroidClient mqttClient) {
        try {
            mqttClient.subscribe(this.context.getResources().getString(R.string.remote_status_topic), 2, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.d("logger", "subscribed");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.d("logger", "failed to subscribe");
                }
            });
        } catch (MqttException ex) {
            ex.printStackTrace();
        }
    }

    // Send command (NEW, START, PAUSE etc.) from Mobile App to RPi via mqtt
    public void sendCommand(String state, String topic, String token) {
        try {
            JSONObject payload = new JSONObject().put("state", state);
            if(topic != null && token != null) {
                payload.put("topic", topic).put("token", token);
            }
            MqttMessage mqttMessage = new MqttMessage(payload.toString().getBytes());
            this.mqttClient.publish(this.context.getResources().getString(R.string.remote_command_topic), mqttMessage);
        } catch (JSONException | MqttException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            this.mqttClient.disconnect();
        } catch (MqttException e) {
            e.printStackTrace();
        }
        this.mqttClient = null;
    }
}
