package george.mobileapp;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class RiderActivity extends AppCompatActivity {
    private MqttHandler mqttHandler;

    private TextView mTxtStatus;
    private TextView mTxtMessage;
    private Button   mBtnNewRide;
    private Button   mBtnStartRide;
    private Button   mBtnPauseRide;
    private Button   mBtnEndRide;

    private boolean appPaused = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rider);

        mTxtStatus = findViewById(R.id.txtStatus);
        mTxtMessage = findViewById(R.id.txtMessage);
        mBtnNewRide = findViewById(R.id.btnNewRide);
        mBtnStartRide = findViewById(R.id.btnStart);
        mBtnPauseRide = findViewById(R.id.btnPause);
        mBtnEndRide = findViewById(R.id.btnEnd);

        //Instantiate a new MqttHandler object if there isn't already an instance
        if(mqttHandler == null)
            mqttHandler = new MqttHandler(this);

        //
        mBtnNewRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject rider = new JSONObject( getIntent().getStringExtra("json_rider") );
                    mqttHandler.sendCommand("NEW", rider.get("topic").toString(), rider.getString("token"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        mBtnStartRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mqttHandler.sendCommand("START", null, null);
            }
        });

        mBtnPauseRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(appPaused) {
                  mqttHandler.sendCommand("RESUME", null, null);
                } else {
                    mqttHandler.sendCommand("PAUSE", null, null);
                }
            }
        });

        mBtnEndRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mqttHandler.sendCommand("STOP", null, null);
            }
        });
    }

    @SuppressLint("SetTextI18n")
    void setState(String state, String message) {
        mTxtStatus.setText(state);
        mTxtMessage.setText(message);

        switch (state){
            case "READY":
                mBtnNewRide.setEnabled(false);
                mBtnStartRide.setEnabled(true);
                mBtnPauseRide.setEnabled(false);
                mBtnPauseRide.setText("Pause Ride");
                mBtnEndRide.setEnabled(false);
                break;
            case "RUNNING":
                appPaused = false;
                mBtnNewRide.setEnabled(false);
                mBtnStartRide.setEnabled(false);
                mBtnPauseRide.setEnabled(true);
                mBtnPauseRide.setText("Pause Ride");
                mBtnEndRide.setEnabled(true);
                break;
            case "PAUSED":
                appPaused = true;
                mBtnNewRide.setEnabled(false);
                mBtnStartRide.setEnabled(false);
                mBtnPauseRide.setEnabled(true);
                mBtnPauseRide.setText("Resume Ride");
                mBtnEndRide.setEnabled(true);
                break;
            case "STOPPED":
                mBtnNewRide.setEnabled(true);
                mBtnStartRide.setEnabled(false);
                mBtnPauseRide.setEnabled(false);
                mBtnPauseRide.setText("Pause Ride");
                mBtnEndRide.setEnabled(false);
                break;
            case "FAILURE":
                mBtnNewRide.setEnabled(false);
                mBtnStartRide.setEnabled(false);
                mBtnPauseRide.setEnabled(true);
                mBtnPauseRide.setText("Pause Ride");
                mBtnEndRide.setEnabled(false);
                this.mqttHandler.close();
                break;
            default:
                mTxtStatus.setText("");
                mTxtMessage.setText("");
                break;
        }
    }
}
