package george.mobileapp;

import org.json.JSONObject;

public interface RestCallback {
    void onSuccess(JSONObject jsonResult);
    void onFailure(JSONObject jsonResult);
    void onNotRider(String message);
}
