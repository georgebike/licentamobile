package george.mobileapp;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class Services {
    private Context context;
    CountDownLatch cdLatch = null;

    Services(Context context) { this.context = context; }

    public void setCdLatch(CountDownLatch cdLatch){
        this.cdLatch = cdLatch;
    }

    public void RequestLogin(String username, String password, final RestCallback callback) {
        JSONObject jsonCredentials = new JSONObject();
        try {
            jsonCredentials.put("username", username);
            jsonCredentials.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonLoginRequest = new JsonObjectRequest(this.context.getResources().getString(R.string.users_api_url) + "login", jsonCredentials, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d("response", response.get("role").toString());
                    // For login success the resp. must have token field and the role as R (rider)
                    if (response.has("token")) {
                        if (response.get("role").toString().equals("R")) callback.onSuccess(response);
                        else callback.onNotRider("You have to login as RIDER");
                    } else {
                        callback.onFailure(response);
                    }
                    cdLatch.countDown();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                VolleyLog.e("Error: " + error.getMessage());
                if (error == null || error.networkResponse == null) return;
                try {
                    String responseBody = new String(error.networkResponse.data, "UTF-8");
                    Toast.makeText(context, responseBody, Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this.context);
        requestQueue.add(jsonLoginRequest);
    }

    public void RequestUserData(final String token, final RestCallback callback) {
        StringRequest getUserRequest = new StringRequest(Request.Method.GET, this.context.getResources().getString(R.string.users_api_url), new Response.Listener<String>() {
            @Override
            public void onResponse(String stringResponse) {
                try {
                    JSONObject response = new JSONObject(stringResponse);
                    Log.d("response", response.get("topic").toString());
                    // If the request was successfull we should find topic field in the response
                    if (response.has("topic")) {
                        callback.onSuccess(response);
                    } else {
                        callback.onFailure(response);
                    }
                    cdLatch.countDown();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error == null || error.networkResponse == null) return;
                try {
                    String responseBody = new String(error.networkResponse.data, "UTF-8");
                    Toast.makeText(context, responseBody, Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("api-token", token);
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this.context);
        requestQueue.add(getUserRequest);
    }
}
