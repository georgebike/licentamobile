package george.mobileapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class LoginActivity extends AppCompatActivity {
    Services services;
    private UserLoginTask mAuthTask;
    private GetUserDataTask mGetUserDataTask;

    private EditText mTxtUsername;
    private EditText mTxtPassword;
    private View     mLoginForm;
    private View     mLoginProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Instantiate the Services class
        services = new Services(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form
        mTxtUsername = findViewById(R.id.txtUsername);
        mTxtPassword = findViewById(R.id.txtPassword);
        Button mBtnLogin = findViewById(R.id.btnLogin);

        mLoginForm   = findViewById(R.id.login_form);
        mLoginProgress = findViewById(R.id.login_progress);

        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });
    }

    private void attemptLogin() {
        // Prevent user from attempting a new login if there is already a task handling the login
        if (mAuthTask != null) {
            return;
        }

        // Reset errors
        mTxtUsername.setError(null);
        mTxtPassword.setError(null);

        // Take the credentials values at the time of the login attempt
        String username = mTxtUsername.getText().toString();
        String password = mTxtPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for valid credentials
        if (!isUsernameValid(username)) {
            mTxtUsername.setError(getString(R.string.error_invalid_username)); // prompt with error message
            focusView = mTxtUsername;
            cancel = true;
        } else if (!isPasswordValid(password)) {
            mTxtPassword.setError(getString(R.string.error_invalid_password));
            focusView = mTxtPassword;
            cancel = true;
        }

        if(cancel) {
            // If an error occured don't attempt login and focus on the form field with the error
            focusView.requestFocus();
        } else {
            // Kick off a background task to perform the user login attempt
            mAuthTask = new UserLoginTask(username, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isUsernameValid(String username) {
        if(TextUtils. isEmpty(username)) {
            return false;
        }
        return true;
    }

    private boolean isPasswordValid(String password) {
        if(TextUtils.isEmpty(password) || password.length()<5) {
            return false;
        }
        return true;
    }

    private class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
        private String mUsername;
        private String mPassword;
        private JSONObject jsonLoginResponse;
        private boolean loginValidated;

        CountDownLatch cdLatch = new CountDownLatch(1);

        UserLoginTask(String username, String password) {
            this.mUsername = username;
            this.mPassword = password;
        }

        @Override
        protected void onPreExecute() {
            // Show the spinner while loading feeds
            mLoginProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            if (services != null) {
                services.setCdLatch(cdLatch);
                services.RequestLogin(mUsername, mPassword, new RestCallback() {
                    @Override
                    public void onSuccess(JSONObject jsonResult) {
                        jsonLoginResponse = jsonResult;
                        loginValidated = true;
                    }

                    @Override
                    public void onFailure(JSONObject jsonResult) {
                        jsonLoginResponse = jsonResult;
                        loginValidated = false;
                    }

                    @Override
                    public void onNotRider(String message) {
                        try {
                            jsonLoginResponse = new JSONObject().put("message", message);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        loginValidated = false;
                    }
                });
                try{
                    cdLatch.await(5000, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return loginValidated;
        }

        @Override
        protected void onPostExecute(final Boolean loginSuccess) {
            mAuthTask = null;
            mLoginProgress.setVisibility(View.GONE);

            if (loginSuccess) {
                if (mGetUserDataTask != null) {
                    return;
                }
                mGetUserDataTask = new GetUserDataTask(jsonLoginResponse);
                mGetUserDataTask.execute((Void) null);
//                Intent riderActivity = new Intent(LoginActivity.this, RiderActivity.class);
//                riderActivity.putExtra("json_rider", jsonLoginResponse.toString());
//                LoginActivity.this.startActivity(riderActivity);
            } else {
                promptMessage(jsonLoginResponse);
            }
        }

        @Override
        protected void onCancelled() { mLoginProgress.setVisibility(View.GONE); mAuthTask = null; }
    }

    private class GetUserDataTask extends AsyncTask<Void, Void, Boolean> {
        JSONObject loginResponse, getUserDataResponse;
        private boolean getDataValidated;
        CountDownLatch cdLatch = new CountDownLatch(1);

        GetUserDataTask(JSONObject loginResponse) {
            this.loginResponse = loginResponse;
        }

        @Override
        protected void onPreExecute() {
            // Show the spinner while loading feeds
            mLoginProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            if (services != null) {
                services.setCdLatch(cdLatch);
                String token = null;
                try {
                    token = loginResponse.get("token").toString();  // Get token from previous login response
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                services.RequestUserData(token, new RestCallback() {
                    @Override
                    public void onSuccess(JSONObject jsonResult) {
                        getUserDataResponse = jsonResult;
                        getDataValidated = true;
                    }

                    @Override
                    public void onFailure(JSONObject jsonResult) {
                        getUserDataResponse = jsonResult;
                        getDataValidated = false;
                    }

                    @Override
                    public void onNotRider(String message) {
                        try {
                            getUserDataResponse = new JSONObject().put("message", message);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        getDataValidated = false;
                    }
                });
                try{
                    cdLatch.await(5000, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return getDataValidated;
        }

        @Override
        protected void onPostExecute(final Boolean getDataSuccess) {
            mGetUserDataTask = null;
            mLoginProgress.setVisibility(View.GONE);

            if (getDataSuccess) {
                Intent riderActivity = new Intent(LoginActivity.this, RiderActivity.class);
                try {
                    getUserDataResponse.put("token", loginResponse.get("token").toString());
                    riderActivity.putExtra("json_rider", getUserDataResponse.toString());
                    LoginActivity.this.startActivity(riderActivity);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "problem assmebling token to json_rider json object", Toast.LENGTH_SHORT).show();
                }
            } else {
                promptMessage(getUserDataResponse);
            }
        }

        @Override
        protected void onCancelled() { mLoginProgress.setVisibility(View.GONE); mAuthTask = null; }
    }

    private void promptMessage(JSONObject failedLoginResponse) {
        final AlertDialog.Builder failedLoginAlert = new AlertDialog.Builder(LoginActivity.this);
        if(failedLoginResponse != null) {
            try {
                failedLoginAlert.setMessage(failedLoginResponse.get("message").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            failedLoginAlert.setMessage("No Response. Connection error");
        }
        failedLoginAlert.setTitle("Error");
        failedLoginAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        failedLoginAlert.setCancelable(true);
        failedLoginAlert.create().show();
    }
}
